/*
 * maven-jetty - Jetty Maven Project Example with Integration Test
 *
 * Copyright (C) 2016-2018 - Thorsten von Bargen
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

package de.vonbargen.mavenjetty;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

class HelloMavenAndJettyServletTest {

    @Test
    void doGet() {
        assertTrue(HelloMavenAndJettyServlet.getGreeting().contains("Maven"), "Greeting contains \"Maven\"");
        assertTrue(HelloMavenAndJettyServlet.getGreeting().contains("Jetty"), "Greeting contains \"Jetty\"");
    }
}
