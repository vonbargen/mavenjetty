/*
 * maven-jetty - Jetty Maven Project Example with Integration Test
 *
 * Copyright (C) 2016-2018 - Thorsten von Bargen
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

package de.vonbargen.mavenjetty;

import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class HalloMavenAndJettyIntegrationTest {

    private static String httpGet(URL url) throws IOException {

        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("GET");

        assertEquals(HttpURLConnection.HTTP_OK, con.getResponseCode(), "Response Code");
        return new BufferedReader(new InputStreamReader(con.getInputStream()))
                .lines().collect(Collectors.joining("\n"));
    }

    @Test
    void main() throws IOException {

        String response = httpGet(new URL("http://localhost:8080/hello"));
        assertTrue(response.contains("session="), "Response contains \"session=\"");
    }

}